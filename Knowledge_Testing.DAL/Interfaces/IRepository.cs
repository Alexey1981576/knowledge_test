﻿using Knowledge_Testing.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Knowledge_Testing.DAL.Interfaces
{
    public interface IRepository : IDisposable
    {
        Question GetQuestion(int Id);
        IEnumerable<Question> GetAllQuestions();
        void CreateQuestion(Question NewQuestion);
        void UpdateQuestion(Question UpdatedQuestion);
        void DeleteQuestion(int Id);
        void Save();
    }   
}
