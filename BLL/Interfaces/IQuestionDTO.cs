﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Interfaces
{
    public interface IQuestionDTO
    {
        int Id { get; set; }
        string TheQuestion { get; set; }
        string Answer_1 { get; set; }
        string Answer_2 { get; set; }
        string Answer_3 { get; set; }
        string Answer_4 { get; set; }
        string RightAnswer { get; set; }
    }
}
