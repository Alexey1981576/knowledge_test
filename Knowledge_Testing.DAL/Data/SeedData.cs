﻿using Knowledge_Testing.DAL.EF;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Knowledge_Testing.DAL.Entities;

namespace Knowledge_Testing.DAL.Data
{
    public static class SeedData
    {
        public static void Initialze(QuestionsDbContext context)
        {
            context.Database.EnsureCreated();
            if(context.Questions.Any())
            {
                return;
            }
            var questions = new Question[]
            {
                new Question{TheQuestion="В каком году родилась София Ротару?", Answer_1="1947", Answer_2="1957", Answer_3="1953", Answer_4="1961", RightAnswer="1947"},
                new Question{TheQuestion="В каком году началась Первая Мировая Война?", Answer_1="1941", Answer_2="1938", Answer_3="1939", Answer_4="1940", RightAnswer="1938"},
                new Question{TheQuestion="В каком году Юрий Гагарин полетел в космос?", Answer_1="1951", Answer_2="1961", Answer_3="он не летал в космос", Answer_4="1981", RightAnswer="1947"},
                new Question{TheQuestion="Кого родила царевна в ночь?", Answer_1="сына", Answer_2="дочь", Answer_3="не то сына не то дочь", Answer_4="не сына и не дочь", RightAnswer="не то сына не то дочь"},
            };
            foreach(var q in questions)
            {
                context.Questions.Add(q);
            }
            context.SaveChanges();
        }
    }
}
