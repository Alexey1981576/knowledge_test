﻿using BLL.DTO;
using BLL.Interfaces;
using Knowledge_Testing.DAL.Interfaces;
using Knowledge_Testing.DAL.Repositories;
using System;
using System.Collections.Generic;

using System.Text;

namespace BLL.Services
{
    public class RepositoryService : IRepositoryService
    {
        IRepository _repository;
        public RepositoryService(IRepository repository)
        {
            _repository = repository;
        }
        public void AddQuestion(QuestionDTO question)
        {
            _repository.CreateQuestion(DTOMapper.TransferDTOtoQuestion(question));
            Save();      
        }

        public void DeleteQuestion(int id)
        {
            _repository.DeleteQuestion(id);
            Save();   
        }

        public IEnumerable<IQuestionDTO> GetAllQuestions()
        {
            return DTOMapper.TransferAllQuestionsToDTO(_repository.GetAllQuestions());
        }

        public QuestionDTO GetQuestionById(int id)
        {
            return DTOMapper.TransferQuestionToDTO(_repository.GetQuestion(id));
        }

        public void UpdateQuestion(QuestionDTO question)
        {
            _repository.UpdateQuestion(DTOMapper.TransferDTOtoQuestion(question));
        }
        public void Save()
        {
            _repository.Save();
        }
    }
}
