﻿using BLL.DTO;
using BLL.Interfaces;
using BLL.Services;
using Knowledge_Testing.DAL.EF;
using Knowledge_Testing.DAL.Entities;
using Knowledge_Testing.DAL.Interfaces;
using Knowledge_Testing.DAL.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Knowledge_Testing.Controllers
{
    [Route("Questions")]
    [ApiController]
    public class QuestionsValuesController : ControllerBase
    {
        IRepositoryService repositoryservice;
        public QuestionsValuesController(IRepositoryService repository)
        {
            this.repositoryservice = repository;
        }
        // GET: api/<QuestionsValuesController>
        //[Authorize]
        [Route("GetQuestions")]
        [HttpGet]
        public IEnumerable<IQuestionDTO> GetQuestions()
        {
            return repositoryservice.GetAllQuestions();
        }

        // GET api/<QuestionsValuesController>/5
        [HttpGet("{id}")]
        public IQuestionDTO Get(int id)
        {

            return repositoryservice.GetQuestionById(id);
        }

        // POST api/<QuestionsValuesController>
        [Authorize]
        [HttpPost]
        public void Post([FromBody] QuestionDTO value)
        {
            repositoryservice.AddQuestion(value);//Метод Save вызывается в Repository на уровне DAL
        }

        // PUT api/<QuestionsValuesController>/5
        [Authorize]
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] QuestionDTO value)
        {
            repositoryservice.UpdateQuestion(value);//Метод Save вызывается в Repository на уровне DAL
        }

        // DELETE api/<QuestionsValuesController>/5
        [Authorize(Roles = "admin")]      
        [HttpDelete("{id}")]
        public string Delete(int id)
        {
            QuestionDTO question = repositoryservice.GetQuestionById(id);
            if(question!=null)
            {
                repositoryservice.DeleteQuestion(id);//Метод Save вызывается в Repository на уровне DAL
                return "succed";
            }
            else
            {
                return "invalid index";
            }
        }
    }
}
