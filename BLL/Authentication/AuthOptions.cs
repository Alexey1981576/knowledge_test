﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;

namespace BLL.Models
{
    public class AuthOptions
    {
        public const string ISSUER = "Knowledge_Testing_Server";
        public const string AUDIENCE = "Knowledge_Testing_Client";
        const string KEY = "supersecretkey!1";
        public const int LIFETIME = 1;
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
