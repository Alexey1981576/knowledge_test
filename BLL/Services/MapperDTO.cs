﻿using BLL.DTO;
using BLL.Interfaces;
using Knowledge_Testing.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Services
{
    public static class DTOMapper
    {
       public static IEnumerable<QuestionDTO> TransferAllQuestionsToDTO(IEnumerable<Question> questions)
        {
            if(questions == null)
            {
                return null;
            }

            List<QuestionDTO> dtoquestions = new List<QuestionDTO>();
            foreach(Question question in questions)
            {
                if (question != null)
                {
                    dtoquestions.Add(TransferQuestionToDTO(question));
                }     
            }
            return dtoquestions;
        }

        public static QuestionDTO TransferQuestionToDTO(Question question)
        {
            if(question == null)
            {
                return null;
            }
            QuestionDTO dtoquestion = new QuestionDTO();
            dtoquestion.Answer_1 = question.Answer_1;
            dtoquestion.Answer_2 = question.Answer_2;
            dtoquestion.Answer_3 = question.Answer_3;
            dtoquestion.Answer_4 = question.Answer_4;
            dtoquestion.Id = question.Id;
            dtoquestion.RightAnswer = question.RightAnswer;
            dtoquestion.TheQuestion = question.TheQuestion;
            return dtoquestion;
        }

        public static Question TransferDTOtoQuestion(QuestionDTO dtoquestion)
        {
            if(dtoquestion == null)
            {
                return null;
            }
            Question question = new Question();
            question.Answer_1 = dtoquestion.Answer_1;
            question.Answer_2 = dtoquestion.Answer_2;
            question.Answer_3 = dtoquestion.Answer_3;
            question.Answer_4 = dtoquestion.Answer_4;
            question.Id = dtoquestion.Id;
            question.RightAnswer = dtoquestion.RightAnswer;
            question.TheQuestion = dtoquestion.TheQuestion;
            return question;
        }

    }
}
