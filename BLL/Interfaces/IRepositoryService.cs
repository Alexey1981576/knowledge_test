﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Interfaces
{
    public interface IRepositoryService
    {
        IEnumerable<IQuestionDTO> GetAllQuestions();
        QuestionDTO GetQuestionById(int id);
        void AddQuestion(QuestionDTO question);
        void DeleteQuestion(int id);
        void UpdateQuestion(QuestionDTO question);
        void Save();
    }
}
