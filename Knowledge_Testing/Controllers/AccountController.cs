﻿using BLL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Knowledge_Testing.Controllers
{
    public class Login
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
    public class AccountController : Controller
    {
        private List<Person> people = new List<Person>()
        {
            new Person{Login="admin@admin.com",Password="admin1",Role="admin"},
            new Person{Login="user@gmail.com",Password="userpass",Role="user"}
        };
        [HttpPost("/token")]
        public IActionResult Token([FromBody] Login user)
        {
            var identity = GetIdentity(user.Username, user.Password);
            if(identity == null)
            {
                return BadRequest(new { errorText = "invalid username or password" });
            }
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256)
                );
            var encodedjwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            var response = new
            {
                acces_token = encodedjwt,
                username = identity.Name
            };
            return Json(response);
        }

        private ClaimsIdentity GetIdentity(string username, string password)
        {
            Person person = people.FirstOrDefault(x => x.Login == username && x.Password == password);
            if(person != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, person.Login),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, person.Role)
                };
                ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, 
                    ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }
            //Если пользователя не найдено
            return null;
        }
    }
}
