﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Knowledge_Testing.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Questions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TheQuestion = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Answer_1 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Answer_2 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Answer_3 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Answer_4 = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RightAnswer = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Questions", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Questions");
        }
    }
}
