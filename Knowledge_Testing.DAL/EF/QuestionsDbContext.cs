﻿using Knowledge_Testing.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Knowledge_Testing.DAL.EF
{
    public class QuestionsDbContext:DbContext
    {
        public DbSet<Question> Questions { get; set; }
        public QuestionsDbContext(DbContextOptions options) : base(options) { }
    }
}
