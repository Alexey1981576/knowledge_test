﻿using Knowledge_Testing.DAL.EF;
using Knowledge_Testing.DAL.Entities;
using Knowledge_Testing.DAL.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Knowledge_Testing.DAL.Repositories
{
    public class QuestionsRepository : IRepository,IDisposable
    {
        private QuestionsDbContext _context;
        public QuestionsRepository(QuestionsDbContext context)
        {
            _context = context;
        }
        public void CreateQuestion(Question NewQuestion)
        {
            _context.Questions.Add(NewQuestion);
        }

        public void DeleteQuestion(int Id)
        {
            Question question = _context.Questions.Find(Id);
            if (question != null)
            {
                _context.Questions.Remove(question);
            }
                
        }

        public IEnumerable<Question> GetAllQuestions()
        {
            return _context.Questions.ToList();
        }

        public Question GetQuestion(int Id)
        {
            return _context.Questions.Find(Id);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void UpdateQuestion(Question UpdatedQuestion)
        {
            _context.Questions.Update(UpdatedQuestion);
            Save();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if(!this.disposed)
            {
                if(disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
