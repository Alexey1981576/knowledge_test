﻿using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class QuestionDTO:IQuestionDTO
    {
        public int Id { get; set; }
        public string TheQuestion { get; set; }
        public string Answer_1 { get; set; }
        public string Answer_2 { get; set; }
        public string Answer_3 { get; set; }
        public string Answer_4 { get; set; }
        public string RightAnswer { get; set; }
    }
}
